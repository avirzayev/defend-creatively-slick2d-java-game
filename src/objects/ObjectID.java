package objects;

public enum ObjectID {
    PLAYER(4),
    PICKAXE(5),
    BLOCK(1),
    BIRCH(1),
    BUTTON(3),
    TEXT(3),
    ITEM(2), STONE(1), WOOL(1), DARK_DIRT(1), LIGHT_DIRT(1), GRASS(1), BEDROCK(1), BIRCH_WALL(0), HAMMER(5), ZOMBIE(4);

    private int renderLevel;

    ObjectID(int renderLevel){
        this.renderLevel = renderLevel;
    }


    public int getRenderLevel(){
        return renderLevel;
    }
}
