package objects;

import Items.Item;
import Items.ItemStatus;
import Items.ItemType;
import Tiled.TileType;
import Tiled.TiledMap;
import game.Calculator;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Block extends Item{

    private boolean breakableNow = false;
    private TiledMap map;
    private int layer;
    private TileType tile;
    private double health;
    private boolean collidable;
    private boolean breakable;

    public Block(int x, int y,TileType tile,TiledMap map,int layer,ItemStatus status) {
        super(x, y, tile.getImage(),tile.getObjectID(),map, ItemType.PLACE,status);
        this.map = map;
        this.layer = layer;
        this.tile = tile;
        this.health = tile.getHealth();
        this.collidable = tile.isColliable();
        this.breakable = tile.isBreakable();
    }

    public Block(int x, int y,TileType tile,TiledMap map,ItemStatus status) {
        super(x, y, tile.getImage(),tile.getObjectID(),map, ItemType.PLACE,status);
        this.map = map;
        this.layer = tile.getLayer();
        this.tile = tile;
        this.health = tile.getHealth();
        this.collidable = tile.isColliable();
        this.breakable = tile.isBreakable();
    }


    @Override
    public GameObject duplicate() {
        return new Block(position.getX(),position.getY(),tile,map,ItemStatus.PICKED);
    }

    public void damage(double damage){
        if (breakable)
            health = Math.max(health - damage,0);

    }

    @Override
    public void onItemPlacedUpdate(int delta) {
        if(health <= 0){
            setItemStatus(ItemStatus.DROPPED);

            int rndX = Calculator.generateRandom(0,5);

            getMap().removeBlock(this);
            position.setX(position.getX() + rndX);
        }
    }

    @Override
    public void onItemPlacedRender(Graphics g) {
        g.drawImage(sprite,position.getX(),position.getY());

        if(breakableNow && isMouseHovering()){
            g.setColor(Color.magenta);
            g.drawRect(position.getX(), position.getY(),31,31);
        }
    }


    public boolean isBreakableNow() {
        return breakableNow;
    }

    public void setBreakableNow(boolean breakableNow) {
        this.breakableNow = breakableNow;
    }

    public TiledMap getMap() {
        return map;
    }

    public void setMap(TiledMap map) {
        this.map = map;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }


    public TileType getTile() {
        return tile;
    }

    public void setTile(TileType tile) {
        this.tile = tile;
    }

    public double getHealth() {
        return health;
    }

    public boolean isCollidable() {
        return collidable;
    }

    public boolean isBreakable() {
        return breakable;
    }
}
