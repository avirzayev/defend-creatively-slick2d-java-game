package objects;

import game.WizardGame;
import grid.Position;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public abstract class GameObject {
    protected Position position;
    protected Image sprite;
    protected int width, height;
    private ObjectID objectID;


    public GameObject(Position position, int width, int height,ObjectID objectID) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.objectID = objectID;
    }

    public GameObject(Position position, Image sprite, ObjectID objectID) {
        this(position,sprite.getWidth(),sprite.getHeight(), objectID);
        this.sprite = sprite;
    }

    public GameObject(int x, int y, Image sprite, ObjectID objectID){
        this(new Position(x,y),sprite, objectID);
    }

    public GameObject(int x, int y, int width, int height, ObjectID objectID) {
        this(new Position(x,y),width,height, objectID);
    }


    public ObjectID getObjectID() {
        return objectID;
    }

    public void setObjectID(ObjectID objectID) {
        this.objectID = objectID;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Image getSprite() {
        return sprite;
    }

    public void setSprite(Image sprite) {
        this.sprite = sprite;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Rectangle getBounds(){
        return new Rectangle(position.getX(),position.getY(),width,height);
    }


    public boolean collidesWith(GameObject object){
        return object.getBounds().intersects(getBounds());

    }

    public boolean collidesWith(int x, int y ,int width, int height){
        return containsPoint(x,y) || containsPoint(x + width,y) || containsPoint(x + width, y + height) || containsPoint(x, y + height);

    }

    public boolean containsPoint(int x, int y){
        return (x > position.getX() && x < position.getX() + width && y > position.getY() && y < position.getY() + height);
    }

    public boolean containsPoint(float x, float y){
        return (x > position.getX() && x < position.getX() + width && y > position.getY() && y < position.getY() + height);
    }

    public boolean containsPoint(Position p){
        return containsPoint(p.getX(),p.getY());
    }

    public boolean isMouseHovering(){
        return containsPoint(WizardGame.getMouseX(),WizardGame.getMouseY());
    }

    public boolean isMouseClicking(int button){
        return isMouseHovering() && WizardGame.app.getInput().isMousePressed(button);
    }

    public abstract void update(int delta);
    public abstract void render(Graphics g);
}
