package grid;

public class Position {

    private float x,y,velX,velY;


    public Position(float x, float y, float velX, float velY) {
        this.x = x;
        this.y = y;
        this.velX = velX;
        this.velY = velY;
    }

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
        this.velX = this.velY = 0;
    }

    @Override
    public String toString() {
        return "Position: (" + x + ", " + y + ")";
    }

    public Position() {
        this.x = this.y = this.velY = this.velX = 0;
    }

    public void move(int delta){

        x += velX * delta/1000f;
        y += velY * delta/1000f;
    }

    public void move(float delta){
        x += velX * delta;
        y += velY * delta;
    }

    public void move(float fixedDeltaX, float fixedDeltaY){
        x += velX * fixedDeltaX;
        y += velY * fixedDeltaY;
    }


    public void multVelX(float mult){
        velX *= mult;
    }

    public void multVelY(float mult){
        velY *= mult;
    }

    public void changeVelXDirection(){
        velX = -velX;
    }

    public void changeVelYDirection(){
        velY = -velY;
    }

    public void addToX(float add){
        x += add;
    }

    public void addToY(float add){
        y += add;
    }

    public void addToVelX(float add){
        velX += add;
    }

    public void addToVelY(float add){
        velY += add;
    }

    public int getX() {
        return (int)x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public int getY() {
        return (int)y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getVelX() {
        return velX;
    }

    public void setVelX(float velX) {
        this.velX = velX;
    }

    public float getVelY() {
        return velY;
    }

    public void setVelY(float velY) {
        this.velY = velY;
    }
}
