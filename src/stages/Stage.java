package stages;


import org.newdawn.slick.Graphics;

public abstract class Stage {
    private StageID stageID;

    public Stage(StageID stageID) {
        this.stageID = stageID;
    }

    protected abstract void stageInit();

    public abstract void update(int delta);

    public abstract void render(Graphics g);

    public void afterRender(Graphics g){}

    protected abstract void stageFinished();

    public StageID getStageID() {
        return stageID;
    }

    public void setStageID(StageID stageID) {
        this.stageID = stageID;
    }

    public void start(){
        stageInit();
    }


}
