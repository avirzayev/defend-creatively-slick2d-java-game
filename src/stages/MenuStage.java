package stages;


import game.Calculator;

import game.Images;
import game.WizardGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import screen_controllers.OnClickListener;
import view.FontSizes;
import view.TextButton;
import view.MainTitle;

import static game.WizardGame.*;

public class MenuStage extends Stage {


    public MenuStage() {
        super(StageID.MENU);

    }

    @Override
    protected void stageInit() {
        generateButtons();

     }

    @Override
    public void update(int delta) {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.lightGray);
        g.fillRect(0,0, 1920,1080);

    }

    @Override
    protected void stageFinished() {

    }

    private void generateTitles(){

        //main title
        handler.add(new MainTitle(0,0, Images.MAIN_TITLE.getImage(0.15f)));

    }

    private void generateButtons(){

        int y = 300;

        FontSizes font  = FontSizes.BIG;

        String[] buttonsNames  = {"Play", "Settings", "Credits", "Quit"};

        OnClickListener[] buttonListener = new OnClickListener[buttonsNames.length];

        buttonListener[0] = new OnClickListener() {
            @Override
            public void update(int delta) {
                WizardGame.setCurrentStage(StageID.GAME);
            }

            @Override
            public void render(Graphics g) {

            }
        };

        buttonListener[1] = new OnClickListener() {
            @Override
            public void update(int delta) {
                System.out.println("Settings....");
            }

            @Override
            public void render(Graphics g) {

            }
        };

        buttonListener[2] = new OnClickListener() {
            @Override
            public void update(int delta) {
                System.out.println("Credits.....");
            }

            @Override
            public void render(Graphics g) {

            }
        };

        buttonListener[3] = new OnClickListener() {
            @Override
            public void update(int delta) {
                System.exit(0);
            }

            @Override
            public void render(Graphics g) {

            }
        };

        for (int i = 0; i < buttonsNames.length; i++) {

            String name = buttonsNames[i];

            TextButton button = new TextButton(name,font, Calculator.getCenterX(0,currentWidth,font.getUniFont().getWidth(name)),y);
            button.setDefaultColor(Color.black);
            button.setHoverColor(Color.white);
            button.setListener(buttonListener[i]);

            handler.add(button);

            y += 150;
        }




    }


}
