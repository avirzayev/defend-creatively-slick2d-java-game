package stages;


import NPCs.Zombie;
import Tiled.TiledMap;
import game.Calculator;
import game.Camera;
import game.WizardGame;
import NPCs.Player;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import screen_controllers.OnClickListener;
import view.FontSizes;
import view.TextButton;

import static game.WizardGame.app;
import static game.WizardGame.handler;

public class GameStage extends Stage{

    private TiledMap map;
    private Player player;


    public GameStage() {
        super(StageID.GAME);
    }

    @Override
    protected void stageInit() {

        TextButton play = new TextButton("Menu", FontSizes.BIG, Calculator.getCenterX(0, app.getScreenWidth(),FontSizes.BIG.getUniFont().getWidth("Menu")),500);
        play.setDefaultColor(Color.black);
        play.setHoverColor(Color.white);
        play.setListener(new OnClickListener() {
            @Override
            public void update(int delta) {
                WizardGame.setCurrentStage(StageID.MENU);
                Camera.reset();
            }

            @Override
            public void render(Graphics g) {

            }
        });
        handler.add(play);

        map = new TiledMap("Resources\\Maps\\Robot Base Map\\Map.json");
        player = new Player(400,400,map);

        handler.add(player);

        handler.add(new Zombie(800,800,map));


    }

    @Override
    public void update(int delta) {
    }

    @Override
    public void render(Graphics g) {

        g.setColor(Color.cyan);
        g.fillRect(0,0,3000,1080);



    }

    @Override
    public void afterRender(Graphics g) {
        player.getInventory().render(g);
    }

    @Override
    protected void stageFinished() {

    }

}
