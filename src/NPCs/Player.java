package NPCs;
import Items.ItemStatus;
import Items.ItemType;
import Tiled.TileType;
import Tiled.TiledMap;
import game.Camera;
import game.Images;
import miningTools.Hammer;
import miningTools.Pickaxe;
import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import view.Inventory;


import static Tiled.TiledMap.TILE_SIZE;
import static game.WizardGame.*;


public class Player extends NPC{

    private Inventory inventory;

    private int scrollValue = 0;




    public Player(int x, int y, TiledMap map) {
        super(x, y, 32, 64,ObjectID.PLAYER,map,500);
        inventory = new Inventory(10, 50,50);
        inventory.addItem(new Block(0,0, TileType.BIRCH,map, ItemStatus.PICKED),100);
        inventory.addItem(new Block(0,0, TileType.STONE,map, ItemStatus.PICKED),100);
        inventory.addItem(new Block(0,0, TileType.GRASS,map, ItemStatus.PICKED),100);
        inventory.addItem(new Block(0,0, TileType.WOOL,map, ItemStatus.PICKED),100);
        inventory.addItem(new Block(0,0, TileType.DARK_DIRT,map, ItemStatus.PICKED),100);
        inventory.addItem(new Block(0,0, TileType.BEDROCK,map, ItemStatus.PICKED),9999);
        inventory.addItem(new Block(0,0, TileType.BIRCH_WALL,map, ItemStatus.PICKED),100);
        inventory.addItem(new Pickaxe(0,0, Images.PICKAXE.getImage(1),ObjectID.PICKAXE,map, ItemType.TOOL,ItemStatus.PICKED, 0.5f,45),1);
        inventory.addItem(new Hammer(0,0, Images.Hammer.getImage(1),ObjectID.HAMMER,map, ItemType.TOOL,ItemStatus.PICKED, 0.5f,25),1);



    }

    @Override
    public void update(int delta) {
        this.delta = delta/1000f;


        scrollValue =  ((Mouse.getDWheel()/120) + scrollValue)%10;
        scrollValue = scrollValue <0? 10 + scrollValue:scrollValue;

        inventory.setPointing(scrollValue);
        inventory.update(delta);
        psychics.applyGravity();

        if (app.getInput().isKeyDown(Input.KEY_A)) position.setVelX(-SPEED);
        else if (app.getInput().isKeyDown(Input.KEY_D)) position.setVelX(SPEED);
        else position.setVelX(0);



        if (app.getInput().isKeyPressed(Input.KEY_W)) psychics.jump();


        psychics.tick(this.delta);
        psychics.checkForCollisions(delta/1000f);
        position.move(delta);



        blockInteractRange = new Rectangle(position.getX() - range*2, position.getY() - range*2, getWidth() + range*4, getHeight() + range*4);


        if (app.getInput().isMouseButtonDown(1)){
            int x = (int)getMouseX();
            int y = (int)getMouseY();


            int pX = position.getX();
            int pY = position.getY();

            int pW = getWidth();
            int pH = getHeight();

            Rectangle playerRect = new Rectangle(pX + 1,pY + 1, pW -2, pH - 2);

            Rectangle rectangle = new Rectangle((int)(x/TILE_SIZE) * TILE_SIZE ,(int)(y/TILE_SIZE) * TILE_SIZE, TILE_SIZE, TILE_SIZE);

            Block block = (Block)inventory.getJustItem();

            if (map.getBlock(block.getLayer(),x/TILE_SIZE,y/TILE_SIZE) == null){
                if (block.getLayer() == 1 || (!rectangle.intersects(playerRect) && blockInteractRange.intersects(rectangle))){
                    block = (Block)inventory.getItem(1);
                    block.getPosition().setX((int)(x/TILE_SIZE) * TILE_SIZE);
                    block.getPosition().setY((int)(y/TILE_SIZE) * TILE_SIZE);
                    block.setItemStatus(ItemStatus.PLACED);
                    handler.add(block);
                    map.addBlock(block.getLayer(),x/TILE_SIZE,y/TILE_SIZE,block);
                }
            }
        }

        if (app.getInput().isMouseButtonDown(0)){

            GameObject ob = inventory.getJustItem();


            if (ob != null){
                if (ob.getObjectID() == ObjectID.PICKAXE){

                    Pickaxe pickaxe = (Pickaxe) ob;
                    int x = (int)getMouseX()/TILE_SIZE;
                    int y = (int)getMouseY()/TILE_SIZE;
                    Block b = map.getBlock(0,x,y);
                    pickaxe.startBreaking(b);

                }else if (ob.getObjectID() == ObjectID.HAMMER){

                    Hammer hammer = (Hammer) ob;
                    int x = (int)getMouseX()/TILE_SIZE;
                    int y = (int)getMouseY()/TILE_SIZE;
                    Block b = map.getBlock(1,x,y);
                    hammer.startBreaking(b);

                }
            }


        }

        if (position.getVelX() > 0) direction = 1;
        else if(position.getVelX() < 0) direction = -1;

        checkForBlocks();

        Camera.tick(this);

    }

    private void checkForBlocks(){

        for(Block block : previousDiggableBlocks){
            block.setBreakableNow(false);
        }
        previousDiggableBlocks.clear();
        GameObject ob = inventory.getJustItem();

        if (ob != null){
            if (inventory.getJustItem().getObjectID() == ObjectID.PICKAXE)
                previousDiggableBlocks =  map.getIntersectingBlocks(blockInteractRange,0);
            else if(inventory.getJustItem().getObjectID() == ObjectID.HAMMER)
                previousDiggableBlocks =  map.getIntersectingBlocks(blockInteractRange,1);
        }


        for(Block block : previousDiggableBlocks){
            block.setBreakableNow(true);
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(255,160,122));

        g.fillRect(position.getX(),position.getY(),width,height);

        g.setColor(Color.black);
        g.drawRect(position.getX(),position.getY(),width,height);

        g.setColor(Color.blue);
        g.draw(blockInteractRange);


    }


    public int getDirection() {
        return direction;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

}
