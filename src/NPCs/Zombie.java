package NPCs;

import Tiled.TiledMap;
import game.WizardGame;
import grid.Position;
import objects.Block;
import objects.ObjectID;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import java.util.LinkedList;


public class Zombie extends NPC {

    private Player player;
    private float hitDelay = 0;

    public Zombie(int x, int y, TiledMap map) {
        super(x, y, 32, 64, ObjectID.ZOMBIE, map, 500);
    }

    @Override
    public void update(int delta) {
        psychics.applyGravity();
        chasePlayer(delta);
        psychics.tick(delta/1000f);
        psychics.checkForCollisions(delta/1000f);
        position.move(delta);
    }

    private void chasePlayer(int delta){
        if (player == null){
            player = (Player) WizardGame.handler.getFirstById(ObjectID.PLAYER);
        }

        Position playerPos = player.getPosition();

        int xDirection = Integer.compare(playerPos.getX(), position.getX());

        position.setVelX((float) (SPEED*0.5*xDirection));

        LinkedList<Block> leftBlocks = map.getLeftBlocks(this,0);
        LinkedList<Block> rightBlocks = map.getRightBlocks(this,0);

        boolean jump = false;


        if (getCollision(psychics.isLeftCollided(), leftBlocks) || getCollision(psychics.isRightCollided(), rightBlocks)){
            jump = psychics.jump();
        }

        if ((psychics.isRightCollided() || psychics.isLeftCollided()) && !jump){
            destroyBlocks(psychics.isLeftCollided(), leftBlocks);
            destroyBlocks(psychics.isRightCollided(), rightBlocks);
        }

        hitDelay += delta/1000f;


    }

    public void destroyBlocks(boolean collision, LinkedList<Block> blocks){
        if (!collision)return;
        if (hitDelay >= 1f){
            hitDelay = 0;
            for (Block b : blocks){
                b.damage(20);
            }
        }

    }

    public boolean getCollision(boolean collision, LinkedList<Block> blocks){
        boolean canJump = blocks.size() > 0;

        for (Block b : blocks) {
            if (b.getPosition().getY() <= getPosition().getY()){
                canJump = false;
            }
        }
        return canJump && collision;
    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(65,94,60));

        g.fillRect(position.getX(),position.getY(),width,height);


        g.setColor(Color.black);
        g.drawRect(position.getX(),position.getY(),width,height);

    }
}
