package NPCs;

import Tiled.TiledMap;
import game.Psychics;
import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import org.newdawn.slick.geom.Rectangle;

import java.util.LinkedList;

public abstract class NPC extends GameObject {

    protected final int range = 1 * 32;
    protected float health;
    protected final float SPEED = 192;
    protected float delta;
    protected int direction = 1;
    protected TiledMap map;
    protected Rectangle blockInteractRange;
    protected Psychics psychics;
    protected LinkedList<Block> previousDiggableBlocks = new LinkedList<>();


    public NPC(int x, int y, int width, int height, ObjectID objectID,TiledMap map, float weight) {
        super(x, y, width, height, objectID);
        psychics = new Psychics(this,50, map);
        this.map = map;
    }


    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }
}
