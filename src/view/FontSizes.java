package view;

import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public enum FontSizes{
    SUPER_SMALL(10),
    SMALLER(20),
    SMALL(30),
    NORMAL(50),
    BIG(70),
    BIGGER(100),
    SUPER_BIG(120);

    private int size;
    private UnicodeFont uniFont;

    FontSizes(int size){
        this.size = size;
        try{
            final java.awt.Font javaFont = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, org.newdawn.slick.util.ResourceLoader.getResourceAsStream("Resources\\font\\johnny fever.ttf"));
            uniFont = new UnicodeFont(javaFont,this.size,false,false);

            uniFont.addAsciiGlyphs();
            uniFont.getEffects().add(new ColorEffect(java.awt.Color.white)); //You can change your color here, but you can also change it in the render{ ... }
            uniFont.addAsciiGlyphs();
            uniFont.loadGlyphs();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public int getSize() {
        return size;
    }


    public UnicodeFont getUniFont() {
        return uniFont;
    }

}
