package view;

import objects.GameObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Animation {

    private GameObject object;

    private Image[] frames;
    private int size;
    private float accumulatedDelta;
    private float delay;
    private int currentPos;


    public Animation(GameObject object, float delay, Image... images){
        this.object = object;
        this.frames = images;
        this.size = frames.length;
        this.delay = delay;
        currentPos = 0;
        accumulatedDelta = 0;
    }


    public void update(int delta){
        accumulatedDelta += delta/1000f;

        if (delay >= accumulatedDelta){
            accumulatedDelta = 0;
            currentPos = (currentPos + 1)%size;
        }

    }

    public void render(Graphics g){
        g.drawImage(frames[currentPos],object.getPosition().getX(),object.getPosition().getY());
    }
}
