package view;

import game.WizardGame;
import grid.Position;
import objects.GameObject;
import objects.ObjectID;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class MainTitle extends GameObject {
    private final int topLimit = 50;
    private final int bottomLimit = 100;


    public MainTitle(Position position, Image sprite) {
        super(position, sprite, ObjectID.TEXT);
        position.setVelY(0.02f);
        position.setY(bottomLimit/2 + 10);
        position.setX((WizardGame.currentWidth - sprite.getWidth())/2f);
    }

    public MainTitle(int x, int y, Image sprite) {
        super(x,y, sprite,ObjectID.TEXT);
        position.setVelY(0.02f);
        position.setY(bottomLimit/2 + 10);
        position.setX((WizardGame.currentWidth - sprite.getWidth())/2f);
    }


    public void scaleSprite(float ratio){
        sprite = sprite.getScaledCopy(ratio);
    }

    @Override
    public void update(int delta) {
        if (position.getY() < topLimit || position.getY() > bottomLimit){
            position.changeVelYDirection();
            if (position.getY() <= topLimit){
                position.setY(topLimit);
            }else{
                position.setY(bottomLimit);
            }
        }

        position.move(delta);
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(sprite,position.getX(),position.getY());
    }
}
