package view;

import objects.GameObject;
import objects.ObjectID;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import screen_controllers.OnClickListener;


public class TextButton extends GameObject {

    private String name;
    private Color defaultColor, hoverColor;
    private FontSizes font;
    private OnClickListener listener;


    public TextButton(String name, FontSizes font, int x, int y) {
        this(name,font,x,y,Color.black,Color.white,null);

    }

    public TextButton(String name, FontSizes font, int x, int y,Color defaultColor) {
        this(name, font, x, y,defaultColor,Color.white,null);

    }

    public TextButton(String name, FontSizes font, int x, int y,Color defaultColor,Color hoverColor) {
        this(name, font, x, y, defaultColor,hoverColor,null);

    }

    public TextButton(String name, FontSizes font, int x, int y,Color defaultColor,Color hoverColor,OnClickListener listener) {
        super(x,y,font.getUniFont().getWidth(name), font.getUniFont().getHeight(name), ObjectID.BUTTON);
        this.name = name;

        this.font = font;
        this.defaultColor = defaultColor;
        this.hoverColor = hoverColor;

        this.listener = listener;

    }



    @Override
    public void update(int delta) {
        if(listener != null && isMouseClicking(0)){
            listener.update(delta);
        }

    }

    @Override
    public void render(Graphics g) {

        font.getUniFont().drawString(position.getX(),position.getY(),name,isMouseHovering()?hoverColor:defaultColor);

        if(listener != null && isMouseClicking(0)){
            listener.render(g);
        }

    }


    public OnClickListener getListener() {
        return listener;
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    public Color getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(Color defaultColor) {
        this.defaultColor = defaultColor;
    }

    public Color getHoverColor() {
        return hoverColor;
    }

    public void setHoverColor(Color hoverColor) {
        this.hoverColor = hoverColor;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
