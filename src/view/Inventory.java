package view;

import Items.Item;
import game.Calculator;
import objects.GameObject;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;


public class Inventory{


    private Item[] items;
    private int[] quantity;
    private int pointing = 0;
    private int renderingX, renderingY;


    public Inventory(int size, int renderingX, int renderingY){

        items = new Item[size];
        quantity = new int[size];

        this.renderingX =renderingX;
        this.renderingY = renderingY;
    }

    public boolean addItem(Item item, int quantity){

        for (int i = 0; i < items.length;i++){
            if (items[i] != null && items[i].getObjectID() == item.getObjectID()){
                this.quantity[i] += quantity;
                return true;
            }
        }

        for(int i = 0 ; i < items.length; i++){
            if (items[i] == null){
                items[i] = item;

                this.quantity[i] = quantity;
                return true;
            }
        }
        return false;
    }

    public void removeCurrentItem() {
        items[pointing] = null;
        quantity[pointing] = 0;
    }

    public void removeQuantity(int index, int quantity){
        if (index >= 0 && index < items.length){
            if (this.quantity[index] >= quantity){
                this.quantity[index] -= quantity;
            }
        }
    }

    public void addQuantity(int index, int quantity){
        if (index > 0 && index < items.length){
            if (this.quantity[index] >= quantity){
                this.quantity[index] += quantity;
            }
        }
    }


    public void update(int delta) {
        for (int i = 0 ; i < items.length;i++){
            if (quantity[i] <= 0 ){
                items[i] = null;
                quantity[i] = 0;
            }
        }

    }

    public GameObject getItem(int index, int i){
        removeQuantity(index,i);
        return items[index].duplicate();

    }


    public GameObject getJustItem(int index, int i){
        return items[index].duplicate();

    }

    public GameObject getItem(int i){
        removeQuantity(pointing,i);
        return items[pointing].duplicate();
    }

    public GameObject getJustItem(){
        if (items[pointing] == null) return null;
        return items[pointing].duplicate();
    }




    public void render(Graphics g) {


        for (int i = 0; i < items.length; i++){

            if (i == pointing){
                g.setColor(new Color(200, 178, 106));
            }else{
                g.setColor(new Color(240, 234, 214));
            }

            g.fillRect(renderingX + i*64, renderingY,64,64);
            g.setColor(Color.black);
            g.drawRect(renderingX + i*64, renderingY,64,64);
            if (items[i] != null){

                g.drawImage(items[i].getSprite(),renderingX + i*64 + 16, renderingY+ 16);
                FontSizes.SMALLER.getUniFont().drawString(Calculator.getCenterX(renderingX + i*64,64,FontSizes.SMALLER.getUniFont().getWidth(quantity[i] + "")), renderingY + 48,quantity[i] + "",Color.black);
            }

        }




    }


    public int getPointing() {
        return pointing;
    }

    public void setPointing(int pointing) {
        this.pointing = pointing;
    }
}
