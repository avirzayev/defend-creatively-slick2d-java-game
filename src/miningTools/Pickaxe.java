package miningTools;

import Items.ItemStatus;
import Items.ItemType;
import Tiled.TiledMap;

import objects.ObjectID;

import org.newdawn.slick.Image;


public class Pickaxe extends MiningTool {



    public Pickaxe(int x, int y, Image sprite, ObjectID objectID, TiledMap map, ItemType type, ItemStatus status,float speed, float damage) {
        super(x, y, sprite, objectID, map, type, status,speed,damage);

    }
}
