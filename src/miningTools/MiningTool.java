package miningTools;

import Items.Item;
import Items.ItemStatus;
import Items.ItemType;
import Tiled.TiledMap;
import game.WizardGame;
import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import NPCs.Player;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class MiningTool extends Item {
    private float damage;
    private float speed;
    private Image rotatedImage;
    private Block block;
    private Player player;
    public static boolean useable = true;
    public static boolean usingTool = false;
    public static final int ROTATION = 140;
    private float rotateSteps;
    private float angle = 0;
    private float deltaSum = 0;


    public MiningTool(int x, int y, Image sprite, ObjectID objectID, TiledMap map, ItemType type, ItemStatus status, float speed, float damage) {
        super(x, y, sprite, objectID, map, type, status);
        player = (Player) WizardGame.handler.getFirstById(ObjectID.PLAYER);
        this.damage = damage;
        rotatedImage = sprite.copy();

        rotatedImage.setCenterOfRotation(sprite.getWidth()/2F, sprite.getHeight());

        rotateSteps = ROTATION/speed;
    }

    @Override
    public GameObject duplicate() {
        return this;
    }

    @Override
    public void onItemPlacedUpdate(int delta) {

        if (player == null){
            player = (Player) WizardGame.handler.getFirstById(ObjectID.PLAYER);
        }

        deltaSum += (delta/1000.0);

        rotatedImage.rotate(-angle);
        angle = rotateSteps*deltaSum* player.getDirection();
        rotatedImage.rotate(angle);

        GameObject ob = player.getInventory().getJustItem();

        if (ob == null || ob.getObjectID() != getObjectID()){
            useable = false;
        }

        if (Math.abs(angle) >= ROTATION){
            if (block != null && useable){
                block.damage(getDamage());
            }
            block = null;
            rotatedImage.rotate(-angle);
            angle = 0;
            WizardGame.handler.remove(this);
            setItemStatus(ItemStatus.PICKED);
            useable = true;
            usingTool = false;
        }


    }

    @Override
    public void onItemPlacedRender(Graphics g) {
        if (useable){
            g.drawImage(rotatedImage,player.getPosition().getX() - 2,player.getPosition().getY() - 8);
        }

    }

    public void startBreaking(Block block){

        if (getItemStatus() == ItemStatus.PICKED && !usingTool){
            this.block = block;
            WizardGame.handler.add(this);
            deltaSum = 0;
            usingTool = true;


            setItemStatus(ItemStatus.PLACED);
        }
    }

    public boolean isBreakingEnabled(){
        return getItemStatus() == ItemStatus.PICKED;
    }


    public float getDamage() {
        return damage;
    }
}
