package Tiled;


import game.Images;
import objects.ObjectID;
import org.newdawn.slick.Image;

import java.util.HashMap;

import static Tiled.TiledMap.TILE_SIZE;

public enum TileType {
    NULL(-1),
    DARK_DIRT(0,ObjectID.DARK_DIRT,50),
    LIGHT_DIRT(1,ObjectID.LIGHT_DIRT,50),
    GRASS(2,ObjectID.GRASS,50),
    BIRCH(3,ObjectID.BIRCH,70),
    STONE(4,ObjectID.STONE,100),
    WOOL(5,ObjectID.WOOL,40),
    BEDROCK(6,ObjectID.BEDROCK,1,true,false,0),
    BIRCH_WALL(7,ObjectID.BIRCH_WALL,80,false,true,1);

    private int id;
    private Image image;
    private ObjectID objectID;
    private static HashMap<Integer, TileType> tiles = new HashMap<>();
    private double health;
    private boolean colliable;
    private int layer;
    private boolean breakable;

    TileType(int id)
    {
        this(id,ObjectID.BLOCK,0, false,false,0);
    }

    TileType(int id, ObjectID objectID,double health){
        this(id,objectID,health, true,true,0);
    }


    TileType(int id, ObjectID objectID,double health, boolean colliable, boolean breakable, int layer){
       this.id = id;
       this.objectID = objectID;
       this.health = health;
       this.colliable =colliable;
       this.breakable = breakable;
       this.layer = layer;
       Image tileset = Images.TILESET.getImage(1);

       final int FIXED_TILE_SIZE = 34;

       if (id != -1){
           image = tileset.getSubImage((id  * FIXED_TILE_SIZE)%tileset.getWidth() ,(id * FIXED_TILE_SIZE)/tileset.getWidth()*FIXED_TILE_SIZE,FIXED_TILE_SIZE,FIXED_TILE_SIZE).getSubImage(1,1,TILE_SIZE,TILE_SIZE);
       }

    }

    static {
        for (TileType tileType: TileType.values()){
            tiles.put(tileType.getId(),tileType);
        }
    }

    public int getId() {
        return id;
    }


    public Image getImage() {
        return image;
    }

    public ObjectID getObjectID() {
        return objectID;
    }

    public static TileType getTileById(int id){
        return tiles.get(id);
    }

    public double getHealth() {
        return health;
    }

    public boolean isColliable() {
        return colliable;
    }

    public boolean isBreakable() {
        return breakable;
    }

    public int getLayer() {
        return layer;
    }
}
