package Tiled;

import Items.ItemStatus;
import game.WizardGame;
import grid.Position;
import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class TiledMap {

    private String path;

    public static final int TILE_SIZE = 32;

    private int width;
    private int height;
    private int gridW, gridH;
    private Block[][][] layers;

    public TiledMap(String path){
        this.path = path;

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path))
        {
            //Read JSON file
            JSONObject obj = (JSONObject)jsonParser.parse(reader);

            this.width = (int)((long)obj.get("width"));
            this.height = (int)((long)obj.get("height"));

            JSONArray layers = (((JSONArray) obj.get("layers")));

            gridW = width;
            gridH = height;

            this.layers = new Block[layers.size()][gridH][gridW];





            width*= TILE_SIZE;
            height*= TILE_SIZE;

            for(int i = 0; i < layers.size(); i++){



                JSONObject obj2 = (JSONObject)(((JSONArray) obj.get("layers")).get(i));

                JSONArray array = (JSONArray)obj2.get("data");

                for (int j = 0; j < gridH*gridW; j++) {


                    int id = (int)(long)array.get(j) -1;

                    int x = (j * TILE_SIZE)%width;
                    int y = (j * TILE_SIZE)/width* TILE_SIZE;


                    int gridX = j%gridW;
                    int gridY = j/gridW;

                    if(TileType.getTileById(id).getImage() != null){
                        Block block = new Block(x,y, TileType.getTileById(id),this,i, ItemStatus.PLACED);
                        this.layers[i][gridY][gridX] = block;
                        WizardGame.handler.add(block);
                    }
                }
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }


    public LinkedList<Block> getRightBlocks(GameObject object,int layer){
        Position position = object.getPosition();

        LinkedList<Block> result = new LinkedList<>();


        int x = position.getX()/TILE_SIZE;
        int y = position.getY()/TILE_SIZE;


        int width = Math.max(object.getWidth()/TILE_SIZE,1);
        int height = Math.max(object.getHeight()/TILE_SIZE,1);

        if (y != (int)Math.ceil((double)position.getY()/TILE_SIZE)){
            height++;
        }

        for (int gridY = y; gridY < y + height; gridY++){
            if (gridY >= 0 && gridY < gridH && x+width >= 0 && x+width < gridW){
                Block block = layers[layer][gridY][x+width];
                if (block == null) continue;

                result.add(block);
            }
        }


        return result;
    }

    public LinkedList<Block> getLeftBlocks(GameObject object,int layer){
        Position position = object.getPosition();

        LinkedList<Block> result = new LinkedList<>();

        int x = position.getX()/TILE_SIZE;
        int y = position.getY()/TILE_SIZE;



        int width = Math.max(object.getWidth()/TILE_SIZE,1);
        int height = Math.max(object.getHeight()/TILE_SIZE,1);

        if (y != (int)Math.ceil((double)position.getY()/TILE_SIZE)){
            height++;
        }

        for (int gridY = y; gridY < y + height; gridY++){
            if (gridY >= 0 && gridY < gridH && x - 1 >= 0 && x - 1 < gridW){
                Block block = layers[layer][gridY][x - 1];
                if (block == null) continue;

                result.add(block);
            }
        }


        return result;
    }

    public LinkedList<Block> getTopBlocks(GameObject object,int layer){
        Position position = object.getPosition();

        LinkedList<Block> result = new LinkedList<>();

        int x = position.getX()/TILE_SIZE;
        int y = position.getY()/TILE_SIZE;

        int width = Math.max(object.getWidth()/TILE_SIZE,1);
        int height = Math.max(object.getHeight()/TILE_SIZE,1);

        if (x != (int)Math.ceil((double)position.getX()/TILE_SIZE)){
            width++;
        }

        for (int gridX = x; gridX < x + width; gridX++){
            if (y - 1 >= 0 && y - 1 < gridH && gridX >= 0 && gridX < gridW){
                Block block = layers[layer][y - 1][gridX];
                if (block == null) continue;

                result.add(block);
            }

        }

        return result;
    }

    public LinkedList<Block> getBottomBlocks(GameObject object,int layer){
        Position position = object.getPosition();

        LinkedList<Block> result = new LinkedList<>();

        int x = position.getX()/TILE_SIZE;
        int y = position.getY()/TILE_SIZE;



        int width = Math.max(object.getWidth()/TILE_SIZE,1);
        int height = Math.max(object.getHeight()/TILE_SIZE,1);

        if (x != (int)Math.ceil((double)position.getX()/TILE_SIZE)){
            // objects that less than 32 pixel will be considered as one tile
            if (object.getWidth() >= TILE_SIZE){
                width++;
            }
        }

        for (int gridX = x; gridX < x + width; gridX++){
            if (y + height >= 0 && y + height < gridH && gridX >= 0 && gridX < gridW) {
                Block block = layers[layer][y + height][gridX];
                if (block == null) continue;

                result.add(block);
            }
        }



        return result;
    }

    public LinkedList<Block> getIntersectingBlocks(Rectangle bounds, int layer){
        int x = (int) (bounds.getX()/TILE_SIZE);
        int y = (int) (bounds.getY()/TILE_SIZE);
        int width = (int) (bounds.getWidth()/TILE_SIZE);
        int height = (int) (bounds.getHeight()/TILE_SIZE);

        LinkedList<Block> result = new LinkedList<>();

        for (int gx = x; gx < x + width;gx++){
            for (int gy = y; gy < y + height; gy++){
                if (gy >= 0 && gy < gridH && gx >= 0 && gx < gridW){
                    Block block = layers[layer][gy][gx];
                    if (block!= null) result.add(block);
                }
            }
        }
        return result;
    }


    public void removeBlock(Block block) {
        Position position = block.getPosition();

        int x = position.getX()/TILE_SIZE;
        int y = position.getY()/TILE_SIZE;

        int layer = block.getLayer();

        if (!(layer < layers.length && y < layers[0].length && x < layers[0][0].length)) return;


        layers[layer][y][x] = null;


    }

    public Block getBlock(int layer, int x, int y){
        if (!(layer < layers.length && y < layers[0].length && x < layers[0][0].length)) return null;
        return layers[layer][y][x];
    }

    public void addBlock(int layer, int x, int y, Block block){
        if (layers[layer][y][x] == null) layers[layer][y][x] = block;
    }
}
