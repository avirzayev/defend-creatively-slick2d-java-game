package Items;

import Tiled.TiledMap;
import game.Calculator;
import game.Psychics;
import game.WizardGame;
import grid.Position;
import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import NPCs.Player;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;



public abstract class Item extends GameObject {
    private Psychics psychics;
    private ItemType type;
    private TiledMap map;
    private ItemStatus itemStatus;
    public static final int collectDistance = 96;

    public Item(Position position, Image sprite, ObjectID objectID, TiledMap map,ItemType type, ItemStatus status) {
        super(position, sprite, objectID);
        psychics = new Psychics(this,200,map);
        this.type = type;
        this.map = map;
        this.itemStatus = status;
    }

    public Item(int x, int y, Image sprite, ObjectID objectID, TiledMap map,ItemType type,ItemStatus status) {
        super(x, y, sprite, objectID);
        psychics = new Psychics(this,200,map);
        this.type = type;
        this.map = map;
        this.itemStatus = status;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public abstract GameObject duplicate();

    @Override
    public void update(int delta) {

        Player player = (Player)WizardGame.handler.getFirstById(ObjectID.PLAYER);


        if (itemStatus == ItemStatus.DROPPED){
            if (width != 24 && height != 24){
                sprite = sprite.getScaledCopy(24,24);
                width = 24;
                height = 24;
            }

            if (Calculator.calculateDistance(getBounds().getCenter(),player.getBounds().getCenter()) < collectDistance){
                int diffX = (int) (position.getX() - player.getBounds().getCenterX());
                int diffY = (int) (position.getY() - player.getBounds().getCenterY());
                int speed = 150;

                float angle = (float)Math.atan2(diffY, diffX);

                position.setVelX((float) (speed * -Math.cos(angle)));
                position.setVelY((float) (speed * -Math.sin(angle)));
            }else{
                Block block = map.getBlock(0,getPosition().getX()/TiledMap.TILE_SIZE,getPosition().getY()/TiledMap.TILE_SIZE);


                if (block != null){
                    getPosition().setY(block.getPosition().getY() - getHeight());
                }

                psychics.applyGravity();
                psychics.tick(delta/1000f);

                psychics.checkForCollisions(delta/1000f);
            }

            if (player.collidesWith(this)){
                player.getInventory().addItem(this,1);
                WizardGame.handler.remove(this);
            }

            getPosition().move(delta);
        }else if (itemStatus == ItemStatus.PLACED){
            onItemPlacedUpdate(delta);
        }

    }

    public abstract void onItemPlacedUpdate(int delta);
    public abstract void onItemPlacedRender(Graphics g);


    public ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Override
    public void render(Graphics g) {
        if (itemStatus == ItemStatus.DROPPED){
            g.drawImage(sprite,getPosition().getX(),getPosition().getY());
        }else if (itemStatus == ItemStatus.PLACED){
            onItemPlacedRender(g);
        }

    }

    public void remove(){
        WizardGame.handler.remove(this);
    }

    public TiledMap getMap() {
        return map;
    }
}
