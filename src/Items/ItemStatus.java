package Items;

public enum ItemStatus {
    DROPPED,
    PLACED,
    PICKED;
}
