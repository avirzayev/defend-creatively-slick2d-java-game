package game;

import objects.Block;
import objects.GameObject;
import objects.ObjectID;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;
import java.util.Comparator;

public class Handler {
    public ArrayList<GameObject> gameObjects = new ArrayList<>();
    private ArrayList<GameObject> frequentObjects = new ArrayList<>();

    public void update(int delta){
        try{
            for (GameObject object : gameObjects) {
                object.update(delta);
            }
        }catch (Exception ignored){
        }
    }



    public void render(Graphics g){
        try{
            gameObjects.sort(Comparator.comparingInt(o -> o.getObjectID().getRenderLevel()));

            for (GameObject object : gameObjects) {
                object.render(g);
            }
        }catch (Exception ignored){
        }
    }

    public void add(GameObject object){
        gameObjects.add(object);
    }

    public void remove(GameObject object){
        gameObjects.remove(object);
        frequentObjects.remove(object);
    }

    public GameObject getFirstById(ObjectID id){
        for (GameObject obj : frequentObjects){
            if (obj.getObjectID() == id) return obj;
        }
        for (GameObject object : gameObjects){
            if (object.getObjectID() == id){
                frequentObjects.add(object);
                return object;
            }

        }
        return null;
    }

    public void clear(){
        gameObjects.clear();
        frequentObjects.clear();
    }
}
