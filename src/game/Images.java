package game;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public enum Images {
    MAIN_TITLE("Text/Entry/Main Title"),
    FONT("font\\font"),
    TILESET("Maps/Robot Base Map/Tileset game"),
    PICKAXE("Maps/Robot Base Map/pick"),
    Hammer("Maps/Robot Base Map/hammer");

    private String path;
    private Image image;

    Images(String path){
        this.path = "/Resources/" + path + ".png";
        try {
            this.image = new Image(this.path);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public Image getImage(float scale){
        return image.getScaledCopy(scale);
    }
}

