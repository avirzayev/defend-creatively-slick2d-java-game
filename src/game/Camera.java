package game;

import grid.Position;
import objects.GameObject;

import static game.WizardGame.WORLD_HEIGHT;
import static game.WizardGame.WORLD_WIDTH;

public class Camera {
    private static float x,y;

    public static void cameraInit(float x, float y) {
        Camera.x = x;
        Camera.y = y;
    }


    public static void tick(GameObject object){
        Position pos = object.getPosition();
        x += ((pos.getX() - x) - WizardGame.app.getScreenWidth()/2f) * 0.02f;
        y += ((pos.getY() - y) - WizardGame.app.getScreenHeight()/2f) * 0.02f;

        x = x <= 0 ? 0:x;
        y = y <= 0 ? 0:y;
        x = x >= WORLD_WIDTH - WizardGame.app.getScreenWidth()? WORLD_WIDTH - WizardGame.app.getScreenWidth():x;
        y = y >= WORLD_HEIGHT - WizardGame.app.getScreenHeight()? WORLD_HEIGHT - WizardGame.app.getScreenHeight():y;
    }

    public static void reset(){
        x = 0;
        y = 0;
    }

    public static float getX() {
        return x;
    }

    public static void setX(float x) {
        Camera.x = x;
    }

    public static float getY() {
        return y;
    }

    public static void setY(float y) {
        Camera.y = y;
    }
}
