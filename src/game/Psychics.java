package game;

import Tiled.TiledMap;
import grid.Position;
import objects.Block;
import objects.GameObject;
import org.newdawn.slick.geom.Rectangle;

import java.util.Arrays;
import java.util.LinkedList;

public class Psychics {
    private GameObject object;
    private float mass;
    private LinkedList<Block> blocks;
    private TiledMap map;

    private float forceX, forceY, accelX, accelY;
    private boolean forceXenabled = false, forceYenabled = false;

    private static final float GRAVITY = 50000;

    private boolean[] collisions;


    public Psychics(GameObject object, float weight, TiledMap map){
        this.object = object;
        this.mass = weight;
        this.map = map;
        collisions = new boolean[4];
    }

    public void checkForCollisions(float delta){
        blocks = new LinkedList<>();

        Rectangle predicted =object.getBounds();
        predicted.setX(object.getPosition().getX() + object.getPosition().getVelX() * delta);
        predicted.setY(object.getPosition().getY() + object.getPosition().getVelY() * delta);

        Arrays.fill(collisions,false);

        for (Block block : map.getTopBlocks(object,0)){
            blocks.add(block);
            if (predicted.getMinY() < block.getBounds().getMaxY()){
                object.getPosition().setY(block.getPosition().getY() + block.getHeight());
                object.getPosition().setVelY(0);
                collisions[0] = true;
                break;
            }
        }

        for (Block block : map.getBottomBlocks(object,0)){
            blocks.add(block);
            if (predicted.getMaxY() > block.getBounds().getMinY()){
                object.getPosition().setY(block.getPosition().getY() - object.getHeight());
                object.getPosition().setVelY(0);
                collisions[1] = true;
                break;
            }
        }

        for (Block block : map.getRightBlocks(object,0)){
            blocks.add(block);
            if (predicted.getMaxX() > block.getBounds().getMinX()){
                object.getPosition().setX(block.getPosition().getX() - object.getWidth());
                object.getPosition().setVelX(0);
                collisions[2] = true;
                break;
            }
        }

        for (Block block : map.getLeftBlocks(object,0)){
            blocks.add(block);

            if (predicted.getMinX() < block.getBounds().getMaxX()){
                object.getPosition().setX(block.getPosition().getX() + block.getWidth());
                object.getPosition().setVelX(0);

                collisions[3] = true;
                break;
            }
        }
    }

    public void tick(float delta){
        Position position = object.getPosition();

        position.addToVelX(accelX*delta);
        position.addToVelY(accelY*delta);

        if ((int)position.getVelX() != 0 && forceXenabled){
            accelX -= ((position.getVelX()/Math.abs(position.getVelX())) * 300) * delta;
        }else if (forceXenabled){
            position.setVelX(0);
            accelX = 0;
            forceXenabled = false;
        }



    }


    public boolean isLeftCollided(){
        return collisions[3];
    }

    public boolean isRightCollided(){
        return collisions[2];
    }

    public boolean isBottomCollided(){
        return collisions[1];
    }

    public boolean isAboveCollided(){
        return collisions[0];
    }



    public void applyGravity(){
        addForceY(GRAVITY);
    }


    public boolean jump(){
        if (isBottomCollided()){
            addForceY(-GRAVITY*40);
        }
        return !isAboveCollided(); // return if jump was successful
    }

    public void setForceX(float force){
        this.forceX = force;
        accelX = forceX/mass;
        forceXenabled = true;
        object.getPosition().setVelX(0);

    }

    public void setForceY(float force){
        this.forceY = force;
        accelY = forceY/mass;
        forceYenabled = true;
        object.getPosition().setVelY(0);
    }

    public void addForceX(float force){
        this.forceX = force;
        accelX = forceX/mass;
        forceXenabled = true;
    }

    public void addForceY(float force){
        this.forceY = force;
        accelY = forceY/mass;
    }

    public LinkedList<Block> getBlocks(){
        return blocks;
    }



}
