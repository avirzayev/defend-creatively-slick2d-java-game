package game;

import java.util.Random;

public class Calculator {
    public static Random rnd = new Random();

    public static int getCenterX(int targetX, int targetWidth, int currentWidth){
        return targetX + (targetWidth-currentWidth)/2;
    }

    public static int generateRandom(int start, int end){
        return rnd.nextInt(end - start + 1) + start;
    }

    public static double calculateDistance(int x1,int y1,int x2,int y2){
        return Math.sqrt(Math.pow(x2 - x1,2) + Math.pow(y2 - y1, 2));
    }

    public static double calculateDistance(float pos1[], float pos2[]){
        return Math.sqrt(Math.pow(pos2[0] -pos1[0],2) + Math.pow(pos2[1] - pos1[1], 2));
    }
}
