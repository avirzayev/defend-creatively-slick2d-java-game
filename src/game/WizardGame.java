package game;

import objects.ObjectID;
import org.newdawn.slick.*;
import stages.GameStage;
import stages.MenuStage;
import stages.Stage;
import stages.StageID;



public class WizardGame extends BasicGame
{

    public static AppGameContainer app;

    public static int currentWidth,currentHeight;

    private static Stage currentStage;

    public static Stage[] stages = new Stage[2];

    public static Handler handler;

    private static float ratioX = 1;
    private static float ratioY = 1;


    public static final int WORLD_WIDTH = 80*32;
    public static final int WORLD_HEIGHT = 34*32;


    public WizardGame()
    {
        super("Wizard game");
    }

    public static void main(String[] arguments)
    {
        try
        {
            currentWidth = 1920;
            currentHeight = 1080;
            app = new AppGameContainer(new WizardGame());
            app.setDisplayMode(currentWidth, currentHeight, false);
            app.setShowFPS(true);
            app.setTargetFrameRate(100);

            app.setAlwaysRender(true);

            app.start();
        }
        catch (SlickException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void init(GameContainer container) throws SlickException
    {

        handler = new Handler();
        stages[0] = new MenuStage();
        stages[1] = new GameStage();
        Camera.cameraInit(0,0);
        currentStage = stages[0];
        currentStage.start();

    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException
    {
        screenListener();


        currentStage.update(delta);
        handler.update(delta);




    }

    public void render(GameContainer container, Graphics g) throws SlickException
    {
        ratioX = (float)app.getWidth()/1920;
        ratioY = (float)app.getHeight()/1080;
        g.scale(ratioX, ratioY);

        g.translate(-Camera.getX(),-Camera.getY());

        currentStage.render(g);
        handler.render(g);

        g.translate(Camera.getX(),Camera.getY());

        currentStage.afterRender(g);



    }


    public void screenListener() throws SlickException {
        if(app.getInput().isKeyPressed(Input.KEY_1)){
            app.setDisplayMode(800,450,false);
        }
        else if(app.getInput().isKeyPressed(Input.KEY_2)){
            app.setDisplayMode(1280,720,false);
        }
        else if(app.getInput().isKeyPressed(Input.KEY_3)){
            app.setDisplayMode(1600,900,false);
        }
        else if(app.getInput().isKeyPressed(Input.KEY_4)){
            app.setDisplayMode(app.getScreenWidth(),app.getScreenHeight(),true);
        }
    }

    public static float getMouseX(){
        return (app.getInput().getMouseX()) / ratioX + Camera.getX();
    }

    public static Stage getCurrentStage() {
        return currentStage;
    }

    public static void setCurrentStage(StageID id) {
        for(Stage stage : stages){
            if(stage.getStageID() == id){
                currentStage = stage;
                break;
            }

        }
        handler.clear();
        currentStage.start();
    }


    public static float getMouseY(){
        return (app.getInput().getMouseY()) / ratioY +  Camera.getY();
    }

}
