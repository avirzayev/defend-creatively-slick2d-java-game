package screen_controllers;

import org.newdawn.slick.Graphics;

public interface OnClickListener {
    public void update(int delta);
    public void render(Graphics g);
}
